Cyber Security Challenge Platform Frontend
=========================================

# Overview

This project contains the frontend for the FH Campus Wien Cyber Security Challenge Platform

# Contribution

Development follows the [git branching model](http://nvie.com/posts/a-successful-git-branching-model/) using the standard prefixes of [git flow](https://github.com/nvie/gitflow).

The format of the Changelog is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

# Usage

## Requirements

* Node Package Manager (npm) is required to setup an environment.
* Install instructions: [https://www.npmjs.com/package/npm]

## Installing development environment
1. Clone the repository `git clone https://gitlab.com/fhcw_itsecurity/cyber_security_platform_frontend.git`
2. Make sure Node Package Manager is installed: `npm -v`
3. To install all dependencies run `npm install` in the working directory
4. Run the webserver by `npm start`
5. Open `http://localhost:8000` in a browser

## Generating a production bundle
1. Install Gulp globally `npm install gulp -g`
2. Install Gulp locally `npm install gulp`
3. Run `gulp default` to generate a productive version
4. Productive version is located in `./release/`
5. Upload the version using `gulp upload`