'use strict';
// Declare app level module which depends on views, and components
angular.module('cscFrontend', [
    'ngRoute',
    'ngCookies',
    'naif.base64',
    'angularUtils.directives.dirPagination',
    'btford.markdown',
    'cscFrontend.view_main',
    'cscFrontend.view_login',
    'cscFrontend.view_welcome',
    'cscFrontend.view_eventList',
    'cscFrontend.view_event',
    'cscFrontend.view_ranking',
    'cscFrontend.view_profile',
    'cscFrontend.view_challenge',
    'cscFrontend.view_resetPassword',
    'cscFrontend.service_authentication',
    'cscFrontend.service_session',
    'cscFrontend.service_notification',
    'cscFrontend.injector_session',
    'cscFrontend.model_user',
    'cscFrontend.model_event',
    'cscFrontend.model_challengeset',
    'cscFrontend.model_challenge',
    'cscFrontend.model_challengeUserDetails',
    'cscFrontend.model_challengeUserPoints',
    'cscFrontend.model_challengeUserSolvingPoints',
    'cscFrontend.model_challengeDescription',
    'cscFrontend.model_challengeHint',
    'cscFrontend.model_challengeHintPointReduction'
])
    .constant('config', {
        apiUrl: 'https://its.fh-campuswien.ac.at/hackathon/backend'
    })
    .config(function (config, $locationProvider, $routeProvider, $httpProvider, $injector, $compileProvider, markdownConverterProvider) {
        $compileProvider.debugInfoEnabled(false);
        $locationProvider.hashPrefix('!');
        $httpProvider.defaults.withCredentials = true;
        $httpProvider.interceptors.push(function($q) {
           return {
               'response': function(response) {
                   return response;
               }
           };
        });
        $httpProvider.interceptors.push('SessionInjector');
        $routeProvider.otherwise({redirectTo: '/view_welcome'});
        markdownConverterProvider.config({
            extensions: [
                function () {
                    return [
                        {
                            type: 'output',
                            regex: '<a(.*?)>',
                            replace: function (match, content) {
                                return '<a target="_blank"' + content + '>';
                            }
                        }
                    ]
                }
            ]
        });
    })
    .controller('MainController', function($scope, $rootScope, $sessionStorage, $route, $routeParams, $location, $cookies, AuthenticationService, SessionService) {
        $scope.$route = $route;
        $scope.$location = $location;
        $scope.$routeParams = $routeParams;
        $rootScope.AuthenticationService = AuthenticationService;
        $rootScope.SessionService = SessionService;
        $rootScope.CookieService = $cookies;
        $rootScope.sessionStorage = $sessionStorage;
        if($cookies.get('user')) {
            $rootScope.SessionService.setUser($cookies.getObject('user'));
        }
        $scope.$on('$routeChangeStart', function(angularEvent, newUrl) {
            if (newUrl.requireAuth && !AuthenticationService.isLoggedIn()) {
                $location.path("/view_login");
            }
            if (newUrl.noAuth && AuthenticationService.isLoggedIn()) {
                $location.path("/view_main");
            }
        });

        $scope.isActive = function (viewLocation) {
            var active = (viewLocation === $location.path());
            return active;
        };
    });