angular.
module('cscFrontend.model_challengeHintPointReduction', ['ngResource']).
factory('ChallengeHintPointReduction', function(config, $resource) {
        return $resource(config.apiUrl + '/challenges/:challengeId/hints/:hintId/pointreduction', {}, {

        });
    }
);