angular.
module('cscFrontend.model_challengeUserPoints', ['ngResource']).
factory('ChallengeUserPoints', function(config, $resource) {
        return $resource(config.apiUrl + '/users/:userId/challenges/:challengeId/points', {}, {
        });
    }
);