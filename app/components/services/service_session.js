'use strict';

angular.module('cscFrontend.service_session', ['ngStorage', 'ngCookies'])
    .factory('SessionService', function($sessionStorage, $cookies) {
        $sessionStorage.user = null;
        return {
            getUser: function() {
                return $sessionStorage.user;
            },
            setUser: function(user) {
                $sessionStorage.user = user;
                $cookies.putObject('user',user);
                return this;
            },
            getAccessToken: function() {
                return $sessionStorage.accessToken;
            },
            setAccessToken: function(accessToken) {
                $sessionStorage.accessToken = accessToken;
            },
            destroy: function() {
                this.setUser(null);
                this.setAccessToken(null);
                $cookies.remove('user');
            }
        };
    });