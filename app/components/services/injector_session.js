'use strict';

angular.module('cscFrontend.injector_session', ['cscFrontend.service_authentication'])
    .factory('SessionInjector', function(SessionService, $location, $q) {
        var sessionInjector = {
            responseError: function(rejection) {
                if (rejection.status == "401") {
                    SessionService.destroy();
                    $location.path("/view_login");
                }
                return $q.reject(rejection);
            }
        };
        return sessionInjector;
});